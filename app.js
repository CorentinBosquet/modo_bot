let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');

let bot = require('./modoBot');
bot.bot();

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

const WebSocket = require('ws');
const https = require("https");
let SocketServer;
let TimerACK;
let IntervalMess;

function CreationWebSocket() {
  SocketServer = new WebSocket("wss://gateway.discord.gg/?v=6&encoding=json");
  SocketServer.on('message', EnvoiDeMessage);
  SocketServer.on('error', ErreurConnexion);
  SocketServer.on('close', FermetureConnexion);

};

const gatewayCo = {
  "heartbeat_interval": 0,
  "op": 0,
  "d": {},
  "s": 0,
  "_trace": ["discord-gateway-prd-1-99"]
};

const SessionID = {
  "token": "NTg1NzIyNjUwMzIwNTY4MzMx.XPdn0Q.Ayy8fZ09zfnRq-QCI0WSd_RD2SI",
  "session_id": "",
  "seq": 0


}

const EnvoiIdentity = {
  "token": "NTg1NzIyNjUwMzIwNTY4MzMx.XPdn0Q.Ayy8fZ09zfnRq-QCI0WSd_RD2SI",
  "properties": {
    "$os": "linux",
      "$browser": "dev_enssat",
      "$device": "dev_enssat"
  }
};


function sendHTTPReq(method, apiPath, id, elem, data) {

    const options = {
        hostname: 'discordapp.com',
        port: 443,
        path: '/api/' + apiPath + '/' + id + '/' + elem,
        method: method,
        headers: {
            "Authorization": "Bot NTg1NzIyNjUwMzIwNTY4MzMx.XPdn0Q.Ayy8fZ09zfnRq-QCI0WSd_RD2SI",
            "User-Agent": "myBotThing (http://some.url, v0.1)",
            "Content-Type": "application/json",
        }
    };

    const req = https.request(options, (res) => {
        console.log('statusCode:', res.statusCode);
        console.log('headers:', res.headers);

        res.on('data', (d) => {
            process.stdout.write(d);
        });
    });

    req.on('error', (e) => {
        console.error(e);
    });
    if (data != null) {
        req.write(data);
    }
    req.end();
}

function sendHTTPReq2(method, apiPath, id, elem) {
    var postData = JSON.stringify({
        'content': 'Pong !'
    });

    var options = {
        hostname: 'discordapp.com',
        port: 443,
        path: '/api/' + apiPath + '/' + id + '/' + elem,
        method: method,
        headers: {
            "Authorization": "Bot NTg1NzIyNjUwMzIwNTY4MzMx.XPdn0Q.Ayy8fZ09zfnRq-QCI0WSd_RD2SI",
            "User-Agent": "myBotThing (http://some.url, v0.1)",
            "Content-Type": "application/json",
            "Content-Length": postData.length,
        }
    };

    var req = https.request(options, (res) => {
        console.log('statusCode:', res.statusCode);
        console.log('headers:', res.headers);

        res.on('data', (d) => {
            process.stdout.write(d);
        });
    });

    req.on('error', (e) => {
        console.error(e);
    });
    req.write(postData);
    req.end();
}

function sendMessage(op, data) {
  const MessageAEnvoyer = {
    "op": op,
    "d": data
  };
  console.log(MessageAEnvoyer);
  SocketServer.send(JSON.stringify(MessageAEnvoyer));
}

function sendHeartbeat() {
  sendMessage(1, {});
  TimerACK = setTimeout(CloseConnexion, 3000);
}

function StockSessionID(sess, sequence) {
  this.session_id = sess;
  this.seq = sequence;
}

function EnvoiDeMessage(message) {

  let mess = JSON.parse(message);
  console.error(mess); //Lecture du message
  if (mess.op === 10 && SessionID.session_id === "") {
    gatewayCo.heartbeat_interval = mess.d.heartbeat_interval;
    IntervalMess = setInterval(sendHeartbeat, mess.d.heartbeat_interval);
    sendMessage(2, EnvoiIdentity);
  } else if (mess.op === 10 && SessionID.session_id !== "") {
    sendMessage(6, SessionID); //RESUME = 6
  }
  if (mess.op === 0) {
    StockSessionID(mess.session_id, mess.sequence);
      if (mess.t === 'MESSAGE_CREATE') {
          if (mess.d.content === '!ping') {
              sendHTTPReq('GET', 'channels', '585719879458357272', 'messages', null);
              sendHTTPReq('POST', 'channels', '585719879458357272', 'messages', JSON.stringify({'content': 'Pong !'}));

              sendHTTPReq2('POST', 'channels', '585719879458357272', 'messages');


          }
      }
  }
    //sendMessage(8,MyMessage);

  if (mess.op === 9) {
    sendMessage(6, SessionID);
  }

  if (mess.op === 11) {
    clearTimeout(TimerACK);
  }
}

function CloseConnexion() {
  clearInterval(IntervalMess);
  clearTimeout(TimerACK);
  SocketServer.close();
}

function ErreurConnexion(error) {
  console.error(error);
}

function FermetureConnexion(CodeFermeture) {
  console.log(CodeFermeture);
  if (CodeFermeture !== 1000) {

    setTimeout(CreationWebSocket, 5000);
  }
}

CreationWebSocket();
