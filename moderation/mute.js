const Discord = require('discord.js');
const db = require('../db.js');


exports.run = (client, message, args) => {
    let nameC = args[0];
    let values = [message.author.id, db.getIDPerm(nameC), message.channel_id]; // TODO mettre dans mute.js
    let userOk = db.getUserRoleOK(values);
    if (userOk == 0) return message.channel.send("Tu ne disposes de ce droit !");

    if (message.mentions.users.size === 0) {
        return message.channel.send('Vous devez mentionner un utilisateur !');
    }
// TODO a voir plus tard
    //  if(message.guild.member(kick).hasPermission("BAN_MEMBERS")) return message.channel.send("Impossible de le mute !")
    if (db.getIsModo().some(item => item.id_moderator === message.mentions.users.first()))
        var mute = message.guild.member(message.mentions.users.first());
    if (!mute) {
        return message.channel.send("Je n'ai pas trouvé l'utilisateur ou il l'existe pas !"); //TODO message.reply
    }
    //TODO timed mute

    let reason;
    if (args[0] == 'Dmute') {
        reason = args.slice(2).join(" ");
    } else {
        reason = args.slice(3).join(" ");
    }
    if (!reason) reason = "Aucune raison n'a été énoncé, c'est la loi du plus fort qui gagne !";
    message.channel.overwritePermissions(mute, {SEND_MESSAGES: false}).then(member => {
        var mute_embed = new Discord.RichEmbed()
            .setColor("#40A497")
            .setTitle("Mute :")
            .addField("Membre muté : ", `${mute.user.username}`)
            .addField("ID : ", `${mute.user.id}`)
            .addField("Modérateur : ", `${message.author.username}`)
            .addField("Raison : ", `${reason}`)
        if (args[2] === '') mute_embed.addField("Until : ", `${args[2]}`)

        client.guilds.get(message.guildID).channels.get(message.channelID).send(mute_embed)
        console.log("Un utilisateur a été mute !")
    });

}