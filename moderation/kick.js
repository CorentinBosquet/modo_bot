const Discord = require('discord.js');
const db = require('../db.js');

exports.run = (client, message, args) => {
    let values = [message.author.id, db.getIDPerm(args[0]), message.channel_id]; // TODO mettre dans mute.js
    let userOk = db.getUserRoleOK(values);

    if (userOk == 0) return message.channel.send("Tu ne disposes de ce droit !");
    if (message.mentions.users.size === 0) {
        return message.channel.send("Vous devez mentionner un utilisateur");
    }

    var kick = message.guild.member(message.mentions.users.first());
    if (!kick) {
        return message.channel.send("L'utilisateur ne se trouve pas sur le serveur !");
    }

    if (db.getIDModo(kick) === undefined) { // TODO a verif
        return message.channel.send("Impossible de l'expulser !");
    }

    kick.kick().then(member => {
        var kick_embed = new Discord.RichEmbed()
            .setColor("#40A497")
            .setTitle("Kick :")
            .addField("Membre kick:", `${member.user.username}`)
            .addField("ID :", `${member.user.id}`)
            .addField("Modérateur :", `${message.author.username}`)
        client.guilds.get(message.guildID).channels.get(message.channelID).send(kick_embed)
        console.log("Un utilisateur a été kick !")
    });


}
