const Discord = require('discord.js');
const db = require('../db.js');
const ms = require('ms');

exports.mute = (client, message, args) => {
    let nameC = args[0];
    /*let values = [message.author.id, db.getIDPerm(nameC), message.channel_id]; // TODO mettre dans verif BDD
    let userOk = db.getUserRoleOK(values);*/
    //if (userOk == 0) return message.channel.send("Tu ne disposes de ce droit !");

    if (message.mentions.users.size === 0) {
        return message.channel.send('Vous devez mentionner un utilisateur !');
    }
// TODO a voir plus tard
    //  if(message.guild.member(kick).hasPermission("BAN_MEMBERS")) return message.channel.send("Impossible de le mute !")
    // if (db.getIsModo().some(item => item.id_moderator === message.mentions.users.first()))
    var mute = message.guild.member(message.mentions.users.first());
    if (!mute) {
        return message.channel.send("Je n'ai pas trouvé l'utilisateur ou il l'existe pas !"); //TODO message.reply
    }
    let reason;
    reason = message.content.match(new RegExp("-r((?:(?!-d|-c).)+)", "i"))
    console.log(reason)
    if (!reason) {
        reason = "Aucune raison n'a été énoncé, c'est la loi du plus fort qui gagne !";
    } else {
        reason = reason[1];
    }
    /*let channels;
    channels = message.content.match(new RegExp("-c((?:(?!-d|-c|-r).)+)", "i"))[1]
    console.log(channels)
    //if (!channels) return message.channel.send( "Aucun channel n'a été énoncé, demande à Jacky le Chan");
*/

    let listC = Array.from(message.mentions.channels.values())
    if (listC[0] === undefined) listC.push(message.channel)
    console.log("listC")
    // console.log(listC[0])
    let nameCh = [];
    listC.forEach(function (ch) {
        console.log("ch : " + ch)
        nameCh.push(ch.name);
        if (ch.type == "voice") {
            let listM = Array.from(ch.members.values())
            if (listM[0] != undefined) {
                listM.forEach(function (m) {
                    if (m.id == message.mentions.users.first().id) {
                        m.setMute(true)
                    }
                })
            }
        } else if (ch.type == "text") {
            ch.overwritePermissions(mute, {SEND_MESSAGES: false, ADD_REACTIONS: false})
        }
    })
    var mute_embed = new Discord.RichEmbed()
        .setColor("#40A497")
        .setTitle("Mute :")
        .addField("Membre muté : ", `${mute.user.username}`)
        .addField("ID : ", `${mute.user.id}`)
        .addField("Modérateur : ", `${message.author.username}`)
        .addField("Raison : ", `${reason}`)
        .addField("Channel(s)", `${nameCh.join(' ')}`)
    console.log(nameCh);

    // TODO regex
    let time;
    time = message.content.match(new RegExp("-d((?:(?!-d|-c|-r).)+)", "i"))
    console.log(time)
    if (time != undefined) {
        time = time[1]
    }//TODO message.reply


    let until
    if (time != undefined) {
        let d = new Date();
        until = parseInt(ms(time.trim()));
        console.log(until)
        mute_embed.addField("Jusqu'au : ", `${new Date(d.getTime() + until)}`);
        console.log("now : " + Date(Date.now()) + " ms(arg2) : " + ms(time.trim()) + " mute pendant :" + new Date(d.getTime() + until) + "type " + typeof (parseInt(ms(until))))
    }

    message.channel.send(mute_embed);

    //message.mentions.users.first().send(mute_embed);
    console.log("Un utilisateur a été mute !")
    let logs = message.guild.channels.find(channel => channel.name === "logs");
    logs.send(mute_embed)

    //console.log(args);
    const mo = require('./moderation.js');

    if (time != undefined) {
        setTimeout(function () {
            mo.unmute(client, message, args);
        }, until)

    }

}

exports.unmute = (client, message, args) => {
    //unmute
    const mo = require('./moderation.js')
    let logs = message.guild.channels.find(channel => channel.name === "logs");


    //message.delete();
    //if(!message.guild.member(message.author).hasPermission("ADMINISTRATOR")) return message.channel.send("Vous n'avez pas la permission !");

    if (message.mentions.users.size === 0) {
        return message.channel.send('Vous devez mentionner un utilisateur !');
    }

    var mute = message.guild.member(message.mentions.users.first());
    if (!mute) {
        return message.channel.send("Je n'ai pas trouvé l'utilisateur ou il l'existe pas !");
    }
    /*let channels;
    channels = message.content.match(new RegExp("-c((?:(?!-d|-c|-r).)+)", "i"))
    console.log(channels)
    if (!channels) return message.channel.send("Aucun channel n'a été énoncé, demande à Jacky le Chan");
*/
    let listC = Array.from(message.mentions.channels.values())
    if (listC[0] === undefined) listC.push(message.channel)
    console.log("listC")
// console.log(listC[0])
    let nameCh = [];
    listC.forEach(function (ch) {
        console.log("ch : " + ch)
        nameCh.push(ch.name);
        if (ch.type == "voice") {
            let listM = Array.from(ch.members.values())
            if (listM[0] != undefined) {
                listM.forEach(function (m) {
                    if (m.id == message.mentions.users.first().id) {
                        m.setMute(false)
                    }
                })
            }
        } else if (ch.type == "text") {
            message.channel.overwritePermissions(mute, {SEND_MESSAGES: true, ADD_REACTIONS: true, SPEAK: true})
        }
    })

    //if(!message.guild.member(client.user).hasPermission("ADMINISTRATOR")) return message.channel.send("Je n'ai pas la permission !");

    message.channel.send(`${mute.user.username} n'est plus mute du channel ${nameCh.join(' ')}!`);
        console.log("Utilisateur unmute !");
    logs.send(`${mute.user.username} n'est plus mute du channel ${nameCh.join(' ')}!`)



}

exports.kick = (client, message, args) => {
    /* let values = [message.author.id,db.getIDPerm(args[0]),message.channel_id]; // TODO mettre dans mute.js
     let userOk = db.getUserRoleOK(values);

     if (userOk==0) return   message.channel.send("Tu ne disposes de ce droit !");*/
    let logs = message.guild.channels.find(channel => channel.name === "logs");

    if (message.mentions.users.size === 0) {
        return message.channel.send("Vous devez mentionner un utilisateur");
    }

    var kick = message.guild.member(message.mentions.users.first());
    if (!kick) {
        return message.channel.send("L'utilisateur ne se trouve pas sur le serveur !");
    }

    /*if (db.getIDModo(kick) === undefined){ // TODO a verif
        return message.channel.send("Impossible de l'expulser !");
    }*/
    let reason;
    reason = message.content.match(new RegExp("-r((?:(?!-d|-c).)+)", "i"))
    console.log(reason)
    if (!reason) {
        reason = "Aucune raison n'a été énoncé, c'est la loi du plus fort qui gagne !";
    } else {
        reason = reason[1];
    }
    kick.kick().then(member => {
        var kick_embed = new Discord.RichEmbed()
            .setColor("#40A497")
            .setTitle("Kick :")
            .addField("Membre kick:", `${member.user.username}`)
            .addField("ID :", `${member.user.id}`)
            .addField("Modérateur :", `${message.author.username}`)
            .addField("Raison : ", `${reason}`)
        message.channel.send(kick_embed)
        console.log("Un utilisateur a été kick !")
        logs.send(kick_embed);
    }).catch(console.error);
}

exports.ban = (client, message, args) => {
    //ban
    console.log("entre dans ban");

    //if(!message.guild.member(message.author).hasPermission("BAN_MEMBERS")) return message.channel.send("Vous n'avez pas la permission");

    if (message.mentions.users.size === 0) {
        return message.channel.send("Vous devez mentionner un utilisateur");
    }

    var ban = message.guild.member(message.mentions.users.first());
    if (!ban) {
        return message.channel.send("L'utilisateur ne se trouve pas sur le serveur !");
    }

    //if(message.guild.member(kick).hasPermission("BAN_MEMBERS")) return message.channel.send("Impossible de le bannir !")

    /*if(!message.guild.member(client.user).hasPermission("BAN_MEMBERS")) {
        return message.channel.send("Je n'ai pas la permission pour ban");
    }*/

    console.log(ban.bannable)
    ban.ban().then(member => {
        var ban_embed = new Discord.RichEmbed()
            .setColor("#a4291d")
            .setTitle("Ban :")
            .addField("Membre banni:", `${ban.user.username}`)
            .addField("ID :", `${ban.user.id}`)
            .addField("Modérateur :", `${message.author.username}`)
            .addField("Raison : ", `${reason}`)


        // TODO regex
        let time;
        time = message.content.match(new RegExp("-d((?:(?!-d|-c|-r).)+)", "i"))
        console.log(time)
        if (time != undefined) {
            time = time[1]
        }

        let until
        if (time != undefined) {
            let d = new Date();
            until = parseInt(ms(time.trim()));
            console.log(until)
            ban_embed.addField("Jusqu'au : ", `${new Date(d.getTime() + until)}`);
            console.log("now : " + Date(Date.now()) + " ms(arg2) : " + ms(time.trim()) + " ban pendant :" + new Date(d.getTime() + until) + "type " + typeof (parseInt(ms(until))))
        }
        if (time != undefined) {
            setTimeout(function () {
                mo.unmute(client, message, args);
            }, until)

        }
        message.channel.send(ban_embed);
        logs.send(ban_embed);
    }).catch(console.error)

    let reason;
    reason = message.content.match(new RegExp("-r((?:(?!-d|-c).)+)", "i"))
    console.log(reason)
    if (!reason) {
        reason = "Aucune raison n'a été énoncé, c'est la loi du plus fort qui gagne !";
    } else {
        reason = reason[1];
    }
    console.log(reason);



    // message.channel.send(ban_embed);

    //message.mentions.users.first().send(mute_embed);
    console.log("Un utilisateur a été ban !")
    let logs = message.guild.channels.find(channel => channel.name === "logs");

    //console.log(args);
    const mo = require('./moderation.js');




    console.log("Un utilisateur a été ban !")


}

exports.warn = (client, message, args) => {
    //ban
    console.log("entre dans warn");
    let logs = message.guild.channels.find(channel => channel.name === "logs");

    //if(!message.guild.member(message.author).hasPermission("BAN_MEMBERS")) return message.channel.send("Vous n'avez pas la permission");

    if (message.mentions.users.size === 0) {
        return message.channel.send("Vous devez mentionner un utilisateur");
    }

    var warn = message.guild.member(message.mentions.users.first());
    if (!warn) {
        return message.channel.send("L'utilisateur ne se trouve pas sur le serveur !");
    }

    //if(message.guild.member(kick).hasPermission("BAN_MEMBERS")) return message.channel.send("Impossible de le bannir !")

    /*if(!message.guild.member(client.user).hasPermission("BAN_MEMBERS")) {
        return message.channel.send("Je n'ai pas la permission pour ban");
    }*/

    let reason;
    reason = message.content.match(new RegExp("-r((?:(?!-d|-c).)+)", "i"))
    console.log(reason);
    if (!reason) {
        return message.channel.send("Avertissement : Aucune raison n'a été énoncé");
    } else {
        reason = reason[1];
    }
    var warn_embed = new Discord.RichEmbed()
        .setColor("#a4291d")
        .setTitle("Warn :")
        //.addField("Membre banni:", `${member.user.username}`)
        .addField("ID :", `${message.mentions.users.first().id}`)
        .addField("Modérateur :", `${message.author.username}`)
        .addField("Raison : ", `${reason}`)

    message.mentions.users.first().send(warn_embed)
    logs.send(warn_embed);
    console.log("Un utilisateur a été warn !")
}

exports.deaf = (client, message, args) => {
    //deaf

    let nameC = args[0];
    /*let values = [message.author.id, db.getIDPerm(nameC), message.channel_id]; // TODO mettre dans verif BDD
    let userOk = db.getUserRoleOK(values);*/
    //if (userOk == 0) return message.channel.send("Tu ne disposes de ce droit !");

    if (message.mentions.users.size === 0) {
        return message.channel.send('Vous devez mentionner un utilisateur !');
    }
// TODO a voir plus tard
    //  if(message.guild.member(kick).hasPermission("BAN_MEMBERS")) return message.channel.send("Impossible de le mute !")
    // if (db.getIsModo().some(item => item.id_moderator === message.mentions.users.first()))
    var deaf = message.guild.member(message.mentions.users.first());
    if (!deaf) {
        return message.channel.send("Je n'ai pas trouvé l'utilisateur ou il l'existe pas !"); //TODO message.reply
    }
    let reason;
    reason = message.content.match(new RegExp("-r((?:(?!-d|-c).)+)", "i"))
    console.log(reason)
    if (!reason) {
        reason = "Aucune raison n'a été énoncé, c'est la loi du plus fort qui gagne !";
    } else {
        reason = reason[1];
    }

    let channels;
    channels = message.content.match(new RegExp("-c((?:(?!-d|-c|-r).)+)", "i"))
    console.log(channels)
    if (!channels) return message.channel.send("Aucun channel n'a été énoncé, demande à Jacky le Chan");

    let listC = Array.from(message.mentions.channels.values())
    if (listC[0] === undefined) listC.push(message.channel)
    console.log("listC")
// console.log(listC[0])
    let nameCh = [];
    listC.forEach(function (ch) {
        console.log("ch : " + ch)
        nameCh.push(ch.name);
        if (ch.type == "voice") {
            let listM = Array.from(ch.members.values())
            if (listM[0] != undefined) {
                listM.forEach(function (m) {
                    if (m.id == message.mentions.users.first().id) {
                        m.setDeaf(true)
                    }
                })
            }
        } else if (ch.type == "text") {
            ch.overwritePermissions(deaf, {READ_MESSAGES: false})
        }
    })
    let deaf_embed = new Discord.RichEmbed()
        .setColor("#40A497")
        .setTitle("Deaf :")
        .addField("Membre deaf : ", `${deaf.user.username}`)
        .addField("ID : ", `${deaf.user.id}`)
        .addField("Modérateur : ", `${message.author.username}`)
        .addField("Raison : ", `${reason}`)
        .addField("Channel(s)", `${nameCh.join(' ')}`)
    console.log(nameCh);

// TODO regex
    let time;
    time = message.content.match(new RegExp("-d((?:(?!-d|-c|-r).)+)", "i"))
    console.log(time)
    if (time != undefined) {
        time = time[1]
    }//TODO message.reply


    let until
    if (time != undefined) {
        let d = new Date();
        until = parseInt(ms(time.trim()));
        console.log(until)
        deaf_embed.addField("Jusqu'au : ", `${new Date(d.getTime() + until)}`);
        console.log("now : " + Date(Date.now()) + " ms(arg2) : " + ms(time.trim()) + " mute pendant :" + new Date(d.getTime() + until) + "type " + typeof (parseInt(ms(until))))
    }

    message.channel.send(deaf_embed);

//message.mentions.users.first().send(mute_embed);
    console.log("Un utilisateur a été mute !")
    let logs = message.guild.channels.find(channel => channel.name === "logs");
    logs.send(deaf_embed)

//console.log(args);
    const mo = require('./moderation.js');

    if (time != undefined) {
        setTimeout(function () {
            mo.unmute(client, message, args);
        }, until)

    }
}

exports.undeaf = (client, message, args) => {
    //unmute
    const mo = require('./moderation.js')
    let logs = message.guild.channels.find(channel => channel.name === "logs");


    //message.delete();
    //if(!message.guild.member(message.author).hasPermission("ADMINISTRATOR")) return message.channel.send("Vous n'avez pas la permission !");

    if (message.mentions.users.size === 0) {
        return message.channel.send('Vous devez mentionner un utilisateur !');
    }

    var deaf = message.guild.member(message.mentions.users.first());
    if (!deaf) {
        return message.channel.send("Je n'ai pas trouvé l'utilisateur ou il l'existe pas !");
    }


    /* let channels;
     channels = message.content.match(new RegExp("-c((?:(?!-d|-c|-r).)+)", "i"))
     console.log(channels)
     if (!channels) return message.channel.send("Aucun channel n'a été énoncé, demande à Jacky le Chan");*/

    let listC = Array.from(message.mentions.channels.values())
    if (listC[0] === undefined) listC.push(message.channel)
    console.log("listC")
// console.log(listC[0])
    let nameCh = [];
    listC.forEach(function (ch) {
        console.log("ch : " + ch)
        nameCh.push(ch.name);
        if (ch.type == "voice") {
            let listM = Array.from(ch.members.values())
            if (listM[0] != undefined) {
                listM.forEach(function (m) {
                    if (m.id == message.mentions.users.first().id) {
                        m.setDeaf(false)
                    }
                })
            }
        } else if (ch.type == "text") {
            ch.overwritePermissions(deaf, {READ_MESSAGES: true})
        }
    })


    //if(!message.guild.member(client.user).hasPermission("ADMINISTRATOR")) return message.channel.send("Je n'ai pas la permission !");

    message.channel.send(`${deaf.user.username} n'est plus deaf !`);
    console.log(`Utilisateur undeaf du channel ${message.channel.name}!`);
    logs.send(`${deaf.user.username} n'est plus deaf du channel ${message.channel.name}!`)


}



