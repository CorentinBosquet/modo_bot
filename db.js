const {Client} = require('pg');

const client = new Client({
    user: 'postgres',
    host: '127.0.0.1',
    database: 'modoBot',
    password: 'postgresql',
    port: 5432,
})
client.connect();

module.exports = {
    getNamePermission: function () {

        return client.query('SELECT name_command from PERMISSION');

    },
    getNamePermission1: async function () {

        let res;
        try {
            res = await client.query('SELECT name_command from PERMISSION');
            return res.rows;
        } catch (e) {
            console.log(e)
        }

    },
    getUserRoleOK: function (values) {
        return // promise
        client.query('SELECT DISTINCT * FROM MODERATION join ATTACHMENT ON moderation.id_role=attachment.id_role  join SCOPE on moderation.id_role=scope.id_role where id_moderator=$1 and id_permission=$2 and SCOPE.id_channel=$3', values)
            .then(res => {
                console.log(res.rows[0])
                return res.rowCount;
            })
            .catch(e => console.error(e.stack))

    },
    getIDPerm: function (values) {
        return client.query('SELECT id_permission from PERMISSION where name_command=$1', values)
            .then(res => {
                console.log(res.rows[0])
                return res.rows;
                // { name: 'brianc', email: 'brian.m.carlson@gmail.com' }
            })
            .catch(e => console.error(e.stack))
    },
    getIsModo: function (namPerm) {
        return client.query('SELECT id_moderator from MODERATOR where id_moderator=$1', values)
            .then(res => {
                console.log(res.rows[0])
                return res.rowCount;
                // { name: 'brianc', email: 'brian.m.carlson@gmail.com' }
            })
            .catch(e => console.error(e.stack))
    }

}